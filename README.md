# CKI Project Container Images

This repo contains the build scripts for the container images used by the CKI
Project. Each container image is built and hosted in GitLab CI using
[buildah](https://github.com/containers/buildah).

## Project Details

### Maintainers

[Iñaki Malerba](https://gitlab.com/inakimalerba)
[Michael Hofmann](https://gitlab.com/mh21)

### Design principles

* Only container image build files (sometimes called *Dockerfiles*) belong in
  the `builds` directory. Any additional content, such as certificates,
  repositories, or configuration files must be in the `files` directory.
* Tags must be in a special date format: `YYYYMMDD.<revision>`. For example,
  the second revision from July 4, 2019 would be tagged `20190704.2`.
* Nearly all containers are based on the latest Fedora release.
* Eventually, this repository should only contain:
  * a `base` container image for derived images
  * a `python` container image for all CI/CD work and the pipeline Python stages
  * various `builder` container images for the pipeline build stages

### Developer guidelines

* All container images are built and pushed with the `latest` tag by default.
  This means that you can push a change and those builds will replace the last
  `latest` tag.
* Almost all CKI projects pull from a specific date tag, such as `20190917.1`,
  so there is very little risk of damaging an active service by applying a new
  tag and pushing container images to that tag. Each CKI project would need to
  be updated to ask for the new tagged container image.
* Keep container images small by cleaning up after yourself within the
  container. Watch out for applications that leave behind lots of cached data.

## GitLab CI building blocks

For setting up GitLab CI in derived project, [gitlab-cki.yml] is provided.

It can be included with something like

```yaml
include:
  - project: 'cki-project/containers'
    file: '/includes/gitlab-cki.yml'
```

and provides the following job templates:
- `.tox`: run tox in the Python image
- `.publish`: publish a container image named after the project
- `.publish_job`: publish a container image named after the job

## Container image building blocks

To build a container image out of an application, a publish job can be added to
`.gitlab-ci.yml` like this:

```yaml
publish:
  extends: .publish
```

Take a look at the [publish job template](includes/gitlab-publish.yml) to see
what variables can be overridden.  The `base_image_tag` and `buildah_image_tag`
variables allow to run tests builds of your application with newer images of
the `base` and `buildah` container images.

Additionally, create the build specification in `builds/application-name.in`.
Build specifications ending in `.in` will be preprocessed with the C
preprocessor `cpp` by `buildah`. This allows to include the following building
blocks with `#include`:

* `setup-base`: `FROM` command for a generic Python application based on the
  `base` container image with support for customizing image name and tag
* `python-requirements`: commands for a generic Python application that
  contains `requirements.txt` and `run.py` files
* `cleanup`: remove caches from `dnf` and `pip`, should always be at the end of
  your build specification

As an example, the build specification for a Python application in
`application-name.in` could look like

```
#include "setup-base"
#include "python-requirements"
#include "cleanup"
```

As the preprocessor will consider anything starting with a `#` as a
preprocessor command, comments need to be put between C-style delimiters like
`/* comment */`.

The `.publish_job` template allows to build multiple container images named
after the jobs with something like

```yaml
backend:
  extends: .publish_job

frontend:
  extends: .publish_job
```

## Building container images

A helper script `cki_build_image.sh` is provided that simplifies building and
pushing images. When run outside of a GitLab pipeline, it will use `buildah`
with the build specification from the `IMAGE_NAME` environment variable to
build an image. In a GitLab pipeline for tags, merge requests and the default
branch, it will push the image to the GitLab container image registry
associated with the project. For a forked project, this will be the container
registry of the fork. For Git tags, the tag name will also be used to tag the
image. For merge requests, a tag of the form `mr-1234` will be used. On the
default branch, `latest` is used.

To run the script outside of the `containers` repository, something like the
following can be used:

```shell
podman run \
    --rm \
    -e IMAGE_NAME=image-name \
    --privileged \
    -w /code \
    -v .:/code \
    -v ~/.local/share/containers:/var/lib/containers \
    registry.gitlab.com/cki-project/containers/buildah:latest \
    cki_build_image.sh
```

When the `OPENSHIFT_REGISTRY` environment variable is defined, the
build will happen as a BuildConfig/Build inside the current context accessible
via `oc`. The built image will be pushed to the given registry by using the
push secret specified in `OPENSHIFT_PUSH_SECRET_NAME`.

When setting the `IMAGE_ARCH` environment variable to a non-empty value, images
can be built for non-native architectures via qemu. The resulting images have
tags with an appended `-arch`. Currently, `amd64`, `arm64`, `ppc64le` and
`s390x` are verified to be compatible with quay.io.

When setting the `IMAGE_ARCHES` environment variable to a stringified list of
architectures, a multi-arch manifest can be built and uploaded. The source
images are downloaded from the registry before creating the manifest to work
around some peculiarities of quay.io.

## Current container images

This repository currently provides the following container images:

### base

This container image is the preferred base image of derived container images
and only includes a minimal Python 3 and pip environment.

### buildah

This container image can run buildah to build container images.

### python

This container image is the preferred image for all CI/CD pipelines in GitLab.
Next to Python 3, it includes everything that is needed to run tests for CKI projects.

### builder-fedora

This container image can compile upstream Linux kernels and it also includes
extra packages for Python.

### builder-rawhide

Similar to `builder-fedora`, but based on a Rawhide environment.

[gitlab-cki.yml]: includes/gitlab-cki.yml
